package com.ramazzotte.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramazzotte.domain.Categoria;
import com.ramazzotte.domain.Lancamento;
import com.ramazzotte.domain.Pessoa;
import com.ramazzotte.domain.TipoLancamento;
import com.ramazzotte.domain.DTO.LancamentoDTO;
import com.ramazzotte.repository.LancamentoRepository;


@Service
public class LancamentoService {
	
	@Autowired
	private LancamentoRepository lancrepo;
	@Autowired
	private CategoriaService serviceCategoria;
	@Autowired
	private PessoaService servicoPessoa;

	public List<LancamentoDTO> findAll() {
		List<Lancamento> list = lancrepo.findAllCat();
		List<LancamentoDTO> listaDTO = new ArrayList<>();
		for(Lancamento l:list) {
			LancamentoDTO lDTO = new  LancamentoDTO(l);
			listaDTO.add(lDTO);
		}

		return listaDTO;
	}

	public Lancamento findPorId(Integer idLancamento) {
		Lancamento cat = lancrepo.findPorId(idLancamento);
		return cat;
	}

	public Lancamento addLancamento(Lancamento cat) {
		return lancrepo.save(cat);
	}

	public void deleteCat(Integer idLancamento) {
		lancrepo.deleteById(idLancamento);
		
	}

	public Lancamento updateLancamento(Lancamento lanc) {
		Integer myInt = Math.toIntExact(lanc.getCodigo());
		Lancamento lancamento = findPorId(myInt);
		lancamento.setDescricao(lanc.getDescricao());
		lancamento.setDataPagamento(lanc.getDataPagamento());
		lancamento.setDataVencimento(lanc.getDataVencimento());
		lancamento.setValor(lanc.getValor());
		lancamento.setObservacao(lanc.getObservacao());
		lancamento.setTipo(lanc.getTipo());
		Categoria categoria  = serviceCategoria.findPorId(lanc.getCategoria().getId());
		lancamento.setCategoria(categoria);
		Pessoa pessoa = servicoPessoa.findPorId(lanc.getPessoa().getCodigo());
		lancamento.setPessoa(pessoa);
		return lancrepo.save(lancamento);
	}








}
