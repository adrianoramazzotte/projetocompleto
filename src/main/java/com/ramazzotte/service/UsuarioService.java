package com.ramazzotte.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramazzotte.domain.Usuario;
import com.ramazzotte.repository.UsuarioRepository;


@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuRepo;

	public List<Usuario> findAll() {
		List<Usuario> list = usuRepo.findAllCat();

		return list;
	}

	public Usuario findPorId(Integer idusuario) {
		Usuario usu = usuRepo.findAllPorId(idusuario);
		return usu;
	}

	public Usuario addusuario(Usuario usu) {
		return usuRepo.save(usu);
	}

	public void deleteUsu(Integer idusuario) {
		usuRepo.deleteById(idusuario);
		
	}

	public Usuario updateusuario(Usuario usuario) {
		Usuario usu = findPorId(usuario.getCodigo());
		usu.setEmail(usuario.getEmail());
		usu.setNome(usuario.getNome());
		usu.setSenha(usuario.getSenha());
		return usuRepo.save(usu);
	}


}
