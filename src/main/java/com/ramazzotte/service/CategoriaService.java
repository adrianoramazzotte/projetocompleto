package com.ramazzotte.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramazzotte.domain.Categoria;
import com.ramazzotte.repository.CategoriaRepository;


@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository catRepo;

	public List<Categoria> findAll() {
		List<Categoria> list = catRepo.findAllCat();

		return list;
	}

	public Categoria findPorId(Integer idcategoria) {
		Categoria cat = catRepo.findPorId(idcategoria);
		return cat;
	}

	public Categoria addCategoria(Categoria cat) {
		return catRepo.save(cat);
	}

	public void deleteCat(Integer idcategoria) {
		catRepo.deleteById(idcategoria);
		
	}

	public Categoria updateCategoria(Categoria categ) {
		Categoria cat = findPorId(categ.getId());
		cat.setNome(categ.getNome());
		return catRepo.save(cat);
	}






}
