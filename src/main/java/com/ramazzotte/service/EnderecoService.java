package com.ramazzotte.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramazzotte.domain.Endereco;
import com.ramazzotte.repository.EnderecoRepository;


@Service
public class EnderecoService {
	
	@Autowired
	private EnderecoRepository endRepo;

	public List<Endereco> findAll() {
		List<Endereco> list = endRepo.findAllCat();

		return list;
	}

	public Endereco findPorId(Integer idendereco) {
		Endereco cat = endRepo.findPorId(idendereco);
		return cat;
	}

	public Endereco addendereco(Endereco cat) {
		return endRepo.save(cat);
	}

	public void deleteEnd(Integer idendereco) {
     System.out.println(idendereco);
     System.out.println("deleteEnd");
		endRepo.deleteById(idendereco);
		
	}

	public Endereco updateendereco(Endereco endereco) {
		Endereco end = findPorId(endereco.getId());
		end.setLogradouro(endereco.getLogradouro());
		end.setNumero(endereco.getNumero());
		end.setComplemento(endereco.getComplemento());
		end.setCep(endereco.getCep());
		end.setBairro(endereco.getBairro());
		end.setCidade(endereco.getCidade());
		end.setEstado(endereco.getEstado());
		return endRepo.save(end);
	}






}
