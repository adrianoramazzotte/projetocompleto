package com.ramazzotte.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramazzotte.domain.Pessoa;
import com.ramazzotte.domain.DTO.PessoaDTO;
import com.ramazzotte.domain.DTO.insert.PessoaInsert;
import com.ramazzotte.repository.PessoaRepository;




@Service
public class PessoaService {
	
	@Autowired
	private PessoaRepository catRepo;
	@Autowired
	private EnderecoService serviceEndereco;

	public List<PessoaDTO> findAll() {
		List<Pessoa> listPessoaBanco = catRepo.findAllPES();
		List<PessoaDTO> listPEssoaDTO = new ArrayList<>();
		for(Pessoa p: listPessoaBanco) {
			PessoaDTO pessoaDTO = new PessoaDTO(p);
			listPEssoaDTO.add(pessoaDTO);
		}

		return listPEssoaDTO;
	}

	public Pessoa findPorId(Integer idpessoa) {
		Pessoa cat = catRepo.findPorId(idpessoa);
		return cat;
	}

	public Pessoa addpessoa(Pessoa cat) {
		return catRepo.save(cat);
	}

	public void deleteCat(Integer idpessoa) {
		catRepo.deleteById(idpessoa);
		
	}
    @Transactional
	public Pessoa updatepessoa(@Valid PessoaInsert pes2) {
		Pessoa pes = findPorId(pes2.getCodigo());
		serviceEndereco.deleteEnd(pes.getEndereco().getId());		
		pes.setNome(pes2.getNome());
		pes.setEndereco(pes2.getEndereco());
    	 catRepo.saveAndFlush(pes);
		return pes;
	}






}
