package com.ramazzotte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoAulaCompletoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoAulaCompletoApplication.class, args);
	}

}
