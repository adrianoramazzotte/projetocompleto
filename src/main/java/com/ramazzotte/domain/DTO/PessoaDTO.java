package com.ramazzotte.domain.DTO;

import com.ramazzotte.domain.Pessoa;

public class PessoaDTO {
	
	private Integer codigo;
	private String nome;
	private Boolean ativo;
	private Integer id;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private String cidade;
	private String estado;
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public PessoaDTO(Integer codigo, String nome, Boolean ativo, Integer id, String logradouro, String numero,
			String complemento, String bairro, String cep, String cidade, String estado) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.ativo = ativo;
		this.id = id;
		this.logradouro = logradouro;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
	}
	public PessoaDTO(Pessoa p) {
		this.codigo = p.getCodigo();
		this.nome = p.getNome();
		this.ativo = p.getAtivo();
		this.id = p.getEndereco().getId();
		this.logradouro = p.getEndereco().getLogradouro();
		this.numero = p.getEndereco().getNumero();
		this.complemento = p.getEndereco().getComplemento();
		this.bairro = p.getEndereco().getBairro();
		this.cep = p.getEndereco().getCep();
		this.cidade = p.getEndereco().getCidade();
		this.estado = p.getEndereco().getEstado();

	}
	
	

}
