package com.ramazzotte.domain.DTO;

import com.ramazzotte.validation.categoria.CategoriaInsert;

@CategoriaInsert
public class CategoriaNewDTO {
	private Integer id;
	private String nome;	
	private String titulo;
	private Boolean status = Boolean.TRUE;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public CategoriaNewDTO(Integer id, String nome, Boolean status) {
		super();
		this.id = id;
		this.nome = nome;
		this.status = status;
	}
	
	public CategoriaNewDTO() {

	}
	

}
