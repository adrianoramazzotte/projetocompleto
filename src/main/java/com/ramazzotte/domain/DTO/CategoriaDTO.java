package com.ramazzotte.domain.DTO;

import com.ramazzotte.validation.categoria.CategoriaUpdate;

@CategoriaUpdate
public class CategoriaDTO {
	private Integer id;
	private String nome;	
	private String titulo;
	private Boolean status = Boolean.TRUE;
	public Integer getId() {
		return id;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CategoriaDTO() {
		super();
	}

	public CategoriaDTO(Integer id, String nome, String titulo, Boolean status) {
		super();
		this.id = id;
		this.nome = nome;
		this.titulo = titulo;
		this.status = status;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	

}
