package com.ramazzotte.domain.DTO;

import com.ramazzotte.validation.usuario.UsuarioUpdate;

@UsuarioUpdate
public class UsuarioDTO {
	
	private Integer codigo;
	private String nome;
	private String email;
	private String senha;
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public UsuarioDTO(Integer codigo, String nome, String email, String senha) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}
	
	public UsuarioDTO() {

	}
	


}
