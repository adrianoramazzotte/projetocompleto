package com.ramazzotte.domain.DTO;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.ramazzotte.domain.Lancamento;
import com.ramazzotte.domain.TipoLancamento;

public class LancamentoDTO {
	private Long codigo;
	private String descricao;
	private LocalDate dataVencimento;
	private LocalDate dataPagamento;
	private BigDecimal valor;
	private String observacao;
	private TipoLancamento tipo;

	private Integer idCategoria;
	private String nomeCategoria;
	private Integer codigoPessoa;
	private String nomePessoa;
	private Boolean ativoPessoa;
	private Integer idPessoaEnd;
	private String logradouroPessoa;
	private String numeroPessoa;
	private String complementoPessoa;
	private String bairroPessoa;
	private String cepPessoa;
	private String cidadePessoa;
	private String estadoPessoa;
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDate getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public LocalDate getDataPagamento() {
		return dataPagamento;
	}
	public void setDataPagamento(LocalDate dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public TipoLancamento getTipo() {
		return tipo;
	}
	public void setTipo(TipoLancamento tipo) {
		this.tipo = tipo;
	}
	public Integer getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	public Integer getCodigoPessoa() {
		return codigoPessoa;
	}
	public void setCodigoPessoa(Integer codigoPessoa) {
		this.codigoPessoa = codigoPessoa;
	}
	public String getNomePessoa() {
		return nomePessoa;
	}
	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}
	public Boolean getAtivoPessoa() {
		return ativoPessoa;
	}
	public void setAtivoPessoa(Boolean ativoPessoa) {
		this.ativoPessoa = ativoPessoa;
	}

	public Integer getIdPessoaEnd() {
		return idPessoaEnd;
	}
	public void setIdPessoaEnd(Integer idPessoaEnd) {
		this.idPessoaEnd = idPessoaEnd;
	}
	public String getLogradouroPessoa() {
		return logradouroPessoa;
	}
	public void setLogradouroPessoa(String logradouroPessoa) {
		this.logradouroPessoa = logradouroPessoa;
	}
	public String getNumeroPessoa() {
		return numeroPessoa;
	}
	public void setNumeroPessoa(String numeroPessoa) {
		this.numeroPessoa = numeroPessoa;
	}
	public String getComplementoPessoa() {
		return complementoPessoa;
	}
	public void setComplementoPessoa(String complementoPessoa) {
		this.complementoPessoa = complementoPessoa;
	}
	public String getBairroPessoa() {
		return bairroPessoa;
	}
	public void setBairroPessoa(String bairroPessoa) {
		this.bairroPessoa = bairroPessoa;
	}
	public String getCepPessoa() {
		return cepPessoa;
	}
	public void setCepPessoa(String cepPessoa) {
		this.cepPessoa = cepPessoa;
	}
	public String getCidadePessoa() {
		return cidadePessoa;
	}
	public void setCidadePessoa(String cidadePessoa) {
		this.cidadePessoa = cidadePessoa;
	}
	public String getEstadoPessoa() {
		return estadoPessoa;
	}
	public void setEstadoPessoa(String estadoPessoa) {
		this.estadoPessoa = estadoPessoa;
	}
	public LancamentoDTO(Long codigo, String descricao, LocalDate dataVencimento, LocalDate dataPagamento,
			BigDecimal valor, String observacao, TipoLancamento tipo, Integer idCategoria, String nomeCategoria,
			Integer codigoPessoa, String nomePessoa, Boolean ativoPessoa, Integer idPessoa, String logradouroPessoa,
			String numeroPessoa, String complementoPessoa, String bairroPessoa, String cepPessoa, String cidadePessoa,
			String estadoPessoa) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.dataVencimento = dataVencimento;
		this.dataPagamento = dataPagamento;
		this.valor = valor;
		this.observacao = observacao;
		this.tipo = tipo;
		this.idCategoria = idCategoria;
		this.nomeCategoria = nomeCategoria;
		this.codigoPessoa = codigoPessoa;
		this.nomePessoa = nomePessoa;
		this.ativoPessoa = ativoPessoa;
		this.idPessoaEnd = idPessoa;
		this.logradouroPessoa = logradouroPessoa;
		this.numeroPessoa = numeroPessoa;
		this.complementoPessoa = complementoPessoa;
		this.bairroPessoa = bairroPessoa;
		this.cepPessoa = cepPessoa;
		this.cidadePessoa = cidadePessoa;
		this.estadoPessoa = estadoPessoa;
	}
	public LancamentoDTO() {
	}
	public LancamentoDTO(Lancamento obj) {
		this.codigo = obj.getCodigo();
		this.descricao = obj.getDescricao();
		this.dataVencimento = obj.getDataVencimento();
		this.dataPagamento = obj.getDataPagamento();
		this.valor = obj.getValor();
		this.observacao = obj.getObservacao();
		this.tipo = obj.getTipo();
		this.idCategoria = obj.getCategoria().getId();
		this.nomeCategoria = obj.getCategoria().getNome();
		this.codigoPessoa = obj.getPessoa().getCodigo();
		this.nomePessoa = obj.getPessoa().getNome();
		this.ativoPessoa = obj.getPessoa().getAtivo();
		this.idPessoaEnd = obj.getPessoa().getEndereco().getId();
		this.logradouroPessoa = obj.getPessoa().getEndereco().getLogradouro();;
		this.numeroPessoa = obj.getPessoa().getEndereco().getNumero();;
		this.complementoPessoa = obj.getPessoa().getEndereco().getComplemento();;
		this.bairroPessoa = obj.getPessoa().getEndereco().getBairro();;
		this.cepPessoa = obj.getPessoa().getEndereco().getCep();;
		this.cidadePessoa = obj.getPessoa().getEndereco().getCidade();;
		this.estadoPessoa = obj.getPessoa().getEndereco().getEstado();;
	}
	
	


}
