package com.ramazzotte.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ramazzotte.domain.Pessoa;
import com.ramazzotte.domain.DTO.PessoaDTO;
import com.ramazzotte.domain.DTO.insert.PessoaInsert;
import com.ramazzotte.service.PessoaService;





@RestController
@RequestMapping(value = "/pessoa")
public class PessoaResource {

	
	@Autowired
	private PessoaService service;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<PessoaDTO> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	@RequestMapping(value = "/{idpessoa}",method = RequestMethod.GET)
	public ResponseEntity<?> findPorId(@PathVariable Integer idpessoa ) {
		Pessoa pessoa = service.findPorId(idpessoa);
		return ResponseEntity.ok().body(pessoa);
		
	}
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> addpessoa(@RequestBody Pessoa cat ) {
		Pessoa pessoa = service.addpessoa(cat);
		return ResponseEntity.ok().body(pessoa);
		
	}
	@RequestMapping(value = "/{idpessoa}",method = RequestMethod.DELETE)
	public void deleteCat(@PathVariable Integer idpessoa ) {
		service.deleteCat(idpessoa);
		
		
	}
	@RequestMapping(value = "/{idpessoa}",method = RequestMethod.PUT)
	public ResponseEntity<?> updatepessoa(
			      @PathVariable Integer idpessoa, 
			      @Valid @RequestBody PessoaInsert pes ) {
		pes.setCodigo(idpessoa);
		Pessoa pessoa = service.updatepessoa(pes);
		return ResponseEntity.ok().body(pessoa);
		
	}



}