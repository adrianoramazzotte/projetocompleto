package com.ramazzotte.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ramazzotte.domain.Endereco;
import com.ramazzotte.service.EnderecoService;



@RestController
@RequestMapping(value = "/endereco")
public class EnderecoResource {

	
	@Autowired
	private EnderecoService service;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<Endereco> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	@RequestMapping(value = "/{idendereco}",method = RequestMethod.GET)
	public ResponseEntity<?> findPorId(@PathVariable Integer idendereco ) {
		Endereco endereco = service.findPorId(idendereco);
		return ResponseEntity.ok().body(endereco);
		
	}
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> addendereco(@RequestBody Endereco end ) {
		Endereco endereco = service.addendereco(end);
		return ResponseEntity.ok().body(endereco);
		
	}
	@RequestMapping(value = "/{idendereco}",method = RequestMethod.DELETE)
	public void deleteCat(@PathVariable Integer idendereco ) {
		service.deleteEnd(idendereco);
		
		
	}
	@RequestMapping(value = "/{idendereco}",method = RequestMethod.PUT)
	public ResponseEntity<?> updateendereco(
			      @PathVariable Integer idendereco, 
			      @RequestBody Endereco xxx ) {
		xxx.setId(idendereco);
		Endereco endereco = service.updateendereco(xxx);
		return ResponseEntity.ok().body(endereco);
		
	}



}