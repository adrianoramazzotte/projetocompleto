package com.ramazzotte.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ramazzotte.domain.Categoria;
import com.ramazzotte.domain.DTO.CategoriaDTO;
import com.ramazzotte.domain.DTO.CategoriaNewDTO;
import com.ramazzotte.service.CategoriaService;



@RestController
@RequestMapping(value = "/categoria")
public class CategoriaResource {

	
	@Autowired
	private CategoriaService service;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<Categoria> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	@RequestMapping(value = "/{idcategoria}",method = RequestMethod.GET)
	public ResponseEntity<?> findPorId(@PathVariable Integer idcategoria ) {
		Categoria categoria = service.findPorId(idcategoria);
		return ResponseEntity.ok().body(categoria);
		
	}
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> addCategoria(@Valid @RequestBody CategoriaNewDTO cat1 ) {
		
		Categoria cat = new Categoria(cat1);
		Categoria categoria = service.addCategoria(cat);
		return ResponseEntity.ok().body(categoria);
		
	}
	@RequestMapping(value = "/{idcategoria}",method = RequestMethod.DELETE)
	public void deleteCat(@PathVariable Integer idcategoria ) {
		service.deleteCat(idcategoria);
		
		
	}
	@RequestMapping(value = "/{idcategoria}",method = RequestMethod.PUT)
	public ResponseEntity<?> updateCategoria(
			      @PathVariable Integer idcategoria, @Valid 
			      @RequestBody CategoriaDTO cat1 ) {
		cat1.setId(idcategoria);
		Categoria cat = new Categoria(cat1);
		Categoria categoria = service.updateCategoria(cat);
		return ResponseEntity.ok().body(categoria);
		
	}



}