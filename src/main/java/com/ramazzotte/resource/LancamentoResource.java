package com.ramazzotte.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ramazzotte.domain.Lancamento;
import com.ramazzotte.domain.DTO.LancamentoDTO;
import com.ramazzotte.service.LancamentoService;




@RestController
@RequestMapping(value = "/lancamento")
public class LancamentoResource {

	
	@Autowired
	private LancamentoService service;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<LancamentoDTO> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	@RequestMapping(value = "/{idLancamento}",method = RequestMethod.GET)
	public ResponseEntity<?> findPorId(@PathVariable Integer idLancamento ) {
		Lancamento lancamento = service.findPorId(idLancamento);
//		LancamentoDTO ldto = new LancamentoDTO(lancamento);
		return ResponseEntity.ok().body(lancamento);
		
	}
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> addLancamento(@RequestBody Lancamento cat ) {
		Lancamento lancamento = service.addLancamento(cat);
		return ResponseEntity.ok().body(lancamento);
		
	}
	@RequestMapping(value = "/{idLancamento}",method = RequestMethod.DELETE)
	public void deleteCat(@PathVariable Integer idLancamento ) {
		service.deleteCat(idLancamento);
		
		
	}
	@RequestMapping(value = "/{idLancamento}",method = RequestMethod.PUT)
	public ResponseEntity<?> updateLanc(@RequestBody Lancamento lanc, @PathVariable Long idLancamento ) {
		lanc.setCodigo(idLancamento);
		Lancamento lancamento = service.updateLancamento(lanc);
		return ResponseEntity.ok().body(lancamento);
		
		
	}
	



}