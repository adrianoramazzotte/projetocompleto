package com.ramazzotte.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ramazzotte.domain.Usuario;
import com.ramazzotte.domain.DTO.UsuarioDTO;
import com.ramazzotte.domain.DTO.UsuarioNewDTO;
import com.ramazzotte.service.UsuarioService;



@RestController
@RequestMapping(value = "/d/usuarios")
public class UsuarioResource {

	
	@Autowired
	private UsuarioService service;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<Usuario> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}	
	@RequestMapping(value = "/{idusuario}",method = RequestMethod.GET)
	public ResponseEntity<?> findPorId(@PathVariable Integer idusuario ) {
		Usuario usuario = service.findPorId(idusuario);
		return ResponseEntity.ok().body(usuario);
		
	}
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> addusuario(@Valid @RequestBody UsuarioNewDTO usu1 ) {
		Usuario usu = new Usuario(usu1);
	     Usuario usuario = service.addusuario(usu);
		return ResponseEntity.ok().body(usuario);
		
	}
	@RequestMapping(value = "/{idusuario}",method = RequestMethod.DELETE)
	public void deleteCat(@PathVariable Integer idusuario ) {
		service.deleteUsu(idusuario);
		
		
	}
	@RequestMapping(value = "/{idusuario}",method = RequestMethod.PUT)
	public ResponseEntity<?> updateusuario(
			      @PathVariable Integer idusuario, @Valid   @RequestBody UsuarioDTO usu1 ) {
		usu1.setCodigo(idusuario);
		Usuario usu = new Usuario(usu1);
		Usuario usuario = service.updateusuario(usu);
		return ResponseEntity.ok().body(usuario);
		
	}



}