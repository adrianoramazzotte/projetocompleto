package com.ramazzotte.validation.categoria;


import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.ramazzotte.domain.Categoria;
import com.ramazzotte.domain.DTO.CategoriaNewDTO;
import com.ramazzotte.repository.CategoriaRepository;
import com.ramazzotte.resource.exception.FieldMessage;





public class CategoriaInsertValidator implements ConstraintValidator<CategoriaInsert, CategoriaNewDTO> {
	@Autowired
	private CategoriaRepository repo;

	
	@Override
	public void initialize(CategoriaInsert ann) {
	}

	@Override
	public boolean isValid(CategoriaNewDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		Categoria aux1 = repo.findByNome(objDto.getNome());
		System.out.println(aux1.getNome());
		if(aux1 !=null) {
			list.add(new FieldMessage("nome"," Nome existente"));
		}
		
		

		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMensagem()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}
