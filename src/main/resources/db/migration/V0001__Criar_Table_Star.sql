
    create table categoria (
       id integer not null auto_increment,
        nome varchar(255) not null,
        primary key (id)
    ) engine=InnoDB default charset=utf8MB4;

    create table endereco (
       id integer not null auto_increment,
        bairro varchar(255),
        cep varchar(255),
        cidade varchar(255),
        complemento varchar(255),
        estado varchar(255),
        logradouro varchar(255),
        numero varchar(255),
        primary key (id)
    ) engine=InnoDB default charset=utf8MB4;

    create table lancamento (
       codigo bigint not null auto_increment,
        data_vencimento date,
        descricao varchar(255),
        observacao varchar(255),
        tipo integer,
        valor decimal(19,2),
        categoria_id integer,
        pessoa_codigo integer,
        primary key (codigo)
    ) engine=InnoDB default charset=utf8MB4;

    create table permissao (
       codigo bigint not null auto_increment,
        descricao varchar(255),
        primary key (codigo)
    ) engine=InnoDB default charset=utf8MB4;

    create table pessoa (
       codigo integer not null auto_increment,
        ativo bit,
        nome varchar(255),
        endereco_id integer,
        primary key (codigo)
    ) engine=InnoDB default charset=utf8MB4;

    create table usuario (
       codigo integer not null auto_increment,
        email varchar(255),
        nome varchar(255),
        senha varchar(255),
        primary key (codigo)
    ) engine=InnoDB default charset=utf8MB4;

    create table usuario_permissao (
       codigo_usuario integer not null,
        codigo_permissao bigint not null
    ) engine=InnoDB default charset=utf8MB4;