alter table lancamento 
       add constraint FKlancamentocategoria
       foreign key (categoria_id) 
       references categoria (id);

    alter table lancamento 
       add constraint FKlancamentoPessoa
       foreign key (pessoa_codigo) 
       references pessoa (codigo);

    alter table pessoa 
       add constraint FKpessoaEndereco
       foreign key (endereco_id) 
       references endereco (id);

    alter table usuario_permissao 
       add constraint FKusuario_permissao_permissao 
       foreign key (codigo_permissao) 
       references permissao (codigo);

    alter table usuario_permissao 
       add constraint FKusuario_permissaoUsuario 
       foreign key (codigo_usuario) 
       references usuario (codigo);
